package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.PurchaseModel;
import org.springframework.http.HttpStatus;

public class PurchaseServiceResponse {

    private String msg;
    private PurchaseModel purchase;
    private HttpStatus httpStatus;

    public PurchaseServiceResponse() {

    }

    public String getMsg() {
        return msg;
    }

    public PurchaseModel getPurchase() {
        return purchase;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setPurchase(PurchaseModel purchase) {
        this.purchase = purchase;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
