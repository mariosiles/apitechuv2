package com.techu.apitechuv2.controllers;


import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.PUT,RequestMethod.POST,RequestMethod.PATCH,RequestMethod.DELETE})
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public List<ProductModel> getProducts(){
        System.out.println("getProducts");

        return this.productService.findAll();
        //return new ArrayList<>();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        //ponemos Object en vez de ProductModel para evitar error en return (en java todoo hereda de object),
        // no es una solución ortodoxa, es para salir del paso, no es recomendado
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        //if (result.isPresent() == true){
        //    return new ResponseEntity<>(result.get(), HttpStatus.OK);
        //}else{
        //  return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        //}

        return new ResponseEntity<>(
            result.isPresent() ? result.get() : "Producto no encontrado",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    // Ini: Mi Código
    @PutMapping("/products")
    public ResponseEntity<Object> putProductById(@RequestBody ProductModel product){
        //ponemos Object en vez de ProductModel para evitar error en return (en java todoo hereda de object),
        // no es una solución ortodoxa, es para salir del paso, no es recomendado

        String id = product.getId();

        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        if (result.isPresent() == true){
            return new ResponseEntity<>(this.productService.saveById(product), HttpStatus.OK);
        }else{
          return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
    }
    // Fin: Mi Código

    // Ini: Código Carlos
    @PutMapping("/products/carlos/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("La descripción del producto  a actualizar es " + product.getDesc());
        System.out.println("El precio del producto  a actualizar es " + product.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if (productToUpdate.isPresent() == true){
            System.out.println("Producto para actualizar encontrado, actualizado");
            this.productService.update(product);
        }

        return new ResponseEntity<>(product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
   // Fin: Código Carlos

    @PostMapping("/products")
    //public ProductModel addProduct(@RequestBody ProductModel product) {
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La descripción del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());

        //return new ProductModel();
        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    // Ini: Mi Código
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProductById(@PathVariable String id){

        System.out.println("deleteProductById");
        System.out.println("La id del producto a borrar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        if (result.isPresent() == true){
            return new ResponseEntity<>(this.productService.deleteById(id), HttpStatus.OK);
        }else{
           return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
    }
    // Fin: Mi Código

    // Ini: Código Carlos
    @DeleteMapping("/products/carlos/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        boolean deletedProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deletedProduct ? "Producto borrado" : "Producto no borrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    // Fin: Código Carlos

}
