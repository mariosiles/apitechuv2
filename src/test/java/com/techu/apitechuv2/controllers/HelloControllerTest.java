package com.techu.apitechuv2.controllers;

import org.junit.Assert;
import org.junit.Test;

public class HelloControllerTest {

    @Test
    public void testHello(){
        HelloController sut = new HelloController(); //sut - system under test

        Assert.assertEquals("Hola Mario!", sut.hello("Mario"));

    }

}
